/**
 * Created by Kevin on 5/14/2016.
 */
(function($) {
    $.fn.extend({
        dialog: function() {
            var $dialog = $(this[0]);
            //noinspection JSValidateTypes
            var $dialogPanel = $dialog.children(".dialog__panel");

            function onDialogClose() {
                $dialog.removeClass("dialog--visible");
                $dialogPanel.unbind("transitionend", onDialogClose);
                $("body").removeClass("noscroll");
            }

            return {
                open: function() {
                    $dialog.addClass("dialog--visible")
                        .addClass("dialog--open");
                    $("body").addClass("noscroll");
                },
                close: function() {
                    $dialog.removeClass("dialog--open");
                    $dialogPanel.on("transitionend", onDialogClose);
                }
            };
        }
    });
})(jQuery);
